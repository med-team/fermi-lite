fermi-lite (0.1+git20221215.85f159e-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Cleanup after testing
    Closes: #1044647
  * Fix watchfile to detect new versions on github (routine-update)
  * Standards-Version: 4.7.0 (routine-update)
  * Do not move header files away from source repository
    Closes: #1049564

 -- Andreas Tille <tille@debian.org>  Sat, 14 Dec 2024 19:47:40 +0100

fermi-lite (0.1+git20190320.b499514-1) unstable; urgency=medium

  [ Michael R. Crusoe ]
  * Team upload.
  * Switch to upstream HEAD using gitmode in watch file
  * Remove trailing whitespace in debian/changelog (routine-update)
  * Use the libsimde-dev package instead of our code copy

  [ Andreas Tille ]
  * Standards-Version: 4.5.1 (routine-update)
  * debhelper-compat 13 (routine-update)

 -- Andreas Tille <tille@debian.org>  Fri, 08 Jan 2021 10:47:39 +0100

fermi-lite (0.1-13) unstable; urgency=medium

  * Fix package to build with gcc-10 (Closes: #957198)

 -- Nilesh Patra <npatra974@gmail.com>  Sat, 18 Apr 2020 22:59:03 +0530

fermi-lite (0.1-12) unstable; urgency=medium

  * Team upload.
  * debian/patches/sync_instead_of_atomic: fix logic and re-enable (fetch the
    previous value, not the new one). Fixes build on mipsel.
  * Added a basic package time test & autopkgtest to confirm the patch using
    fml-asm

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Mon, 30 Mar 2020 10:55:37 +0200

fermi-lite (0.1-11) unstable; urgency=medium

  [ Steve Langasek ]
  * Ensure that our variable checking for >= 0 is signed
    Closes: #954127

 -- Andreas Tille <tille@debian.org>  Sat, 28 Mar 2020 19:33:46 +0100

fermi-lite (0.1-10) unstable; urgency=medium

  * Team upload.
  * Revert change: Fix compatibility on mipsel, m68k, powerpc, and sh4 by using
    __sync instead of __atomic from 0.1-8 since it breaks ariba
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Andreas Tille <tille@debian.org>  Thu, 12 Mar 2020 14:23:57 +0100

fermi-lite (0.1-9) unstable; urgency=medium

  * Team upload.
  * Mark libfml{0,-dev} as Multi-Arch: same
  * Add armel to the -latomic group

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Sat, 15 Feb 2020 13:58:17 +0100

fermi-lite (0.1-8) unstable; urgency=medium

  * Team upload.
  * Fix compatibility on mipsel, m68k, powerpc, and sh4 by using __sync instead
    of __atomic
  * Standards-Version: 4.5.0 (routine-update)
  * debhelper-compat 12 (routine-update)

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Sat, 15 Feb 2020 13:18:41 +0100

fermi-lite (0.1-7) unstable; urgency=medium

  * Team upload.
  * Add -latomic for those archs that need it

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Sat, 14 Dec 2019 15:33:59 +0100

fermi-lite (0.1-6) unstable; urgency=medium

  * Team upload.
  * Enable for all architectures using simde
  * Ship a symbols file

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Fri, 13 Dec 2019 18:04:33 +0100

fermi-lite (0.1-5) unstable; urgency=medium

  [ Steffen Möller ]
  * Add new d/u/metadata file

  [ Sascha Steinbiss ]
  * Update Vcs-* fields with Salsa addresses.
  * Bump Standards-Version.
  * Remove unnecessary Testsuite field.
  * Use debhelper 11.
  * Improve hardening.

 -- Sascha Steinbiss <satta@debian.org>  Wed, 04 Jul 2018 23:40:50 +0200

fermi-lite (0.1-4) unstable; urgency=medium

  * Upload to unstable

 -- Andreas Tille <tille@debian.org>  Sun, 18 Jun 2017 08:12:35 +0200

fermi-lite (0.1-3) experimental; urgency=medium

  * Team upload.
  * Avoid name space conflict with bwa

 -- Andreas Tille <tille@debian.org>  Thu, 02 Feb 2017 10:59:28 +0100

fermi-lite (0.1-2) unstable; urgency=medium

  * Autopkgtest: build example outside of upstream source tree.
  * Restrict architectures to those with SSE2 support.
    See also upstream's comment at https://github.com/lh3/fermi-lite/issues/4

 -- Sascha Steinbiss <satta@debian.org>  Thu, 04 Aug 2016 05:37:30 +0000

fermi-lite (0.1-1) unstable; urgency=low

  * Initial packaging (Closes: #832757)

 -- Sascha Steinbiss <satta@debian.org>  Thu, 28 Jul 2016 22:26:11 +0000
